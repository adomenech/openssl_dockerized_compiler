## What?

This is a dockerfile to create a container for building Centos7 rpm package from opessl release 1.0.1e withe the eliptic curve function secp256k1 activated.

Basically the containers dowloads the source code RPM for the release and we add fedora specific patch to activate secp256k1 on compilation time

## Fine, what I need?

Just Docker and create an output dir wherever you download this files.

## The lazy how to:

```Bash
./compile.sh
```

## How in detail:

The detailed way to generate the rpm is:

1 - Generating the container image: downloads all the sources needed

```Bash
docker build -t openssl_compiler .
```

2 - Executing the container image: compilation and rpm generation

```Bash
docker run --name opensslc -v $(pwd)/output:/root/rpmbuild/RPMS openssl_compiler

```

3 - Clean the dust

```Bash
docker rm -f opensslc
docker rmi -f openssl_compiler

```

Enjoy

**ado**