FROM centos:centos7
MAINTAINER Albert Domenech (adomenech@cnmc.es)

ENV \
	OPENSSL_VERSION=1.0.1e

RUN yum update -y && \
	yum install -y net-tools deltarpm \
					wget curl-devel git \
					zlib-devel \
					openssl-devel \
					pcre pcre-devel \
					make gcc gcc-c++ \
					glibc libcom_err \
					krb5-devel \
					rpm-build && \
	useradd -m builder 

WORKDIR /root

RUN	mkdir rpmbuild

RUN mkdir /output && \
	cd rpmbuild && \
	git clone git://git.centos.org/rpms/openssl . && \
	git checkout -b c7 origin/c7 && \
	git branch -D master && \
	mkdir BUILD && \
	mkdir RPMS && \
	mkdir SRPMS && \
	cd SOURCES && \
	wget http://pkgs.fedoraproject.org/repo/pkgs/openssl/openssl-1.0.1e-hobbled.tar.xz/6115ae0bb61b481a9195baef72514c2e/openssl-1.0.1e-hobbled.tar.xz && \
	wget https://www.bfccomputing.com/downloads/fedora/openssl/secp256k1/ec_curve.c.patch && \
	wget https://www.bfccomputing.com/downloads/fedora/openssl/secp256k1/openssl-1.0.1e-ecc-suiteb.patch.patch && \
	patch -p1 openssl-1.0.1e-ecc-suiteb.patch < openssl-1.0.1e-ecc-suiteb.patch.patch && \
	patch -p1 ec_curve.c < ec_curve.c.patch

VOLUME /root/rpmbuild/RPMS

CMD ["/usr/bin/rpmbuild", "-ba", "/root/rpmbuild/SPECS/openssl.spec"]