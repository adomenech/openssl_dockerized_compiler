#!/bin/bash
set -e

docker build -t openssl_compiler .
docker run --name opensslc -v $(pwd)/output:/root/rpmbuild/RPMS openssl_compiler
docker rm -f opensslc
#docker rmi -f openssl_compiler
sudo chown -R ${USER}:${USER} $(pwd)/output

exit 0